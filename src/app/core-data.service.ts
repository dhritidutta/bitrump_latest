import {
  Injectable
} from '@angular/core';
import * as $ from 'jquery';
import {
  Router
} from '@angular/router';
import {
  UserIdleService
} from 'angular-user-idle';
import { HttpClient } from '@angular/common/http';
import { ViewChild, ElementRef } from '@angular/core';
import {OrderBookComponent} from '../app/order-book/order-book.component'
@Injectable({
  providedIn: 'root'
})
export class CoreDataService {

  /* Change here */
  CURRENCYICON: any = '$ ';
  CURRENCYNAME: any = 'USD';
  exchange: any = 'Bitrump';
  logo: any = './assets/img/bitrump-new-logo.png';

  /* environment: any = 'dev';
   ADDR = "http://52.53.237.92:8080/main.html#!/";*/

  environment: any = 'live';
  ADDR = "http://52.53.237.92:8080/main.html#!/";
  /*Change here*/
  loader: boolean = false;
  reason: any;
  // count: any = 0;
  selectedBuyingCryptoCurrencyName: string;
  selectedSellingCryptoCurrencyName: string;
  selectedCryptoCurrencySymbol: string;
  triggersBalance: any = 0;
  selectedBuyingAssetText: string;
  selectedSellingAssetText: string;
  environment_settings_list: string;
  //new
  selectedBuyingCryptoCurrencyissuer: string;
  selectedSellingCryptoCurrencyissuer: string;
  ctpdata: string;
  lowprice: string;
  highprice: string;
  changescreencolor: boolean = false;
  rocdata;
  act;
  react;
  cur;
  ACTION;
  rocreact;
  negetive;
  volumndata;
  indicatorGroup1: any = ['ATR', 'BBAND', 'MACD', 'EMA', 'ROC', 'KDJ', 'MFI', 'CMF'];
  indicatorGroup2: any = ['ARN', 'CHO', 'HA', 'KCH', 'SSMA', 'SOSC', 'Willams %r', 'TRIX'];

  interval: any;
  time: any;
  url: boolean = false;
  resize: boolean = false;
  icon: any;
  WEBSERVICE: string;
  REPORTSERVISE: string;
  TRADESERVICE: string;
  CHARTSERVISE: string;
  //malini CURRENCYSERVISE
  CURRENCYSERVISE: string;
  //end
  settings: any;
  count: any;
  source2:any;

  constructor(private route: Router, private userIdle: UserIdleService, private http: HttpClient) {

    var alertPl = `<div class="alertPlace"></div>`;
    $('html').append(alertPl).fadeIn();

    switch (this.environment) {
      case 'live':
        this.WEBSERVICE = "https://api.bitrump.com/api";
        this.TRADESERVICE = "https://api.bitrump.com/TradeAmount/rest/MyAmount";
        this.CHARTSERVISE = "https://api.bitrump.com/TrendSpriceVolume/paybito/";//live
        this.REPORTSERVISE = "https://api.bitrump.com/PaybitoReportModule/report/";
        break;

    }

    this.getSettings();
  }

  getSettings() {
    this.http.get<any>('./assets/settings.json')
      .subscribe(data => {
        this.settings = data;
      })
  }

  // Login Session Management
  idleLogout() {
   
    var us = localStorage.getItem('access_token');
    if (us != null) {
      this.userIdle.onTimerStart();
      this.userIdle.startWatching();
      this.userIdle.resetTimer();
      
     // this.time=10;
      // this.userIdle.onTimerStart().subscribe(count => 
      //   console.log(count));
      this.userIdle.onTimerStart().subscribe(count => {
        //  console.log(this.time);
      //  console.log('count', count);
      //  console.log('time', this.time);
        if (count == 1) {
          $('#logoutWarn').click();
        }
        else if (count == 10) {

          $('#logoutWarncl').click();

          this.logout();
        }
        this.count = count;
       // this.count=this.time;
        //this.time --;
      });
      
      this.userIdle.ping$.subscribe(ping => {
       
      }
      );
      this.userIdle.onTimeout().subscribe(() => {
      //  console.log('Time is up!');

      // alert('hgk');
        $('#logoutWarncl').click();
        
        this.logout();
      }
      );

    }

  }
  startWatching() {
    this.userIdle.startWatching();
  }
  stopWatching() {
    this.userIdle.stopWatching();
  }
  Stop() {
    // alert('bmnb');
    $('#logoutWarncl').click();
    $('.d-block').removeClass('show');
    $('.d-block').siblings().removeClass("modal-backdrop");

    this.userIdle.stopTimer();
    this.userIdle.stopWatching();
    this.userIdle.resetTimer();
    // this.restart();
    var presentRefreshToken = localStorage.getItem('refresh_token');
    //console.log('localstorage',presentRefreshToken)

    var fd = new FormData();
    fd.append('refresh_token', presentRefreshToken);
    fd.append('grant_type', 'refresh_token');
    //this.data.Stop();
    this.http.post<any>(this.WEBSERVICE + '/oauth/token?grant_type=refresh_token&refresh_token=' + presentRefreshToken, '', {
      headers: {
        // 'Content-Type': 'application/x-www-form-urlencoded',
        'authorization': 'Basic cGF5Yml0by13ZWItY2xpZW50OlB5Z2h0bzM0TEpEbg=='
      }
    })
      .subscribe(response => {
        var result = response;
        localStorage.setItem('access_token', result.access_token);
        localStorage.setItem('refresh_token', result.refresh_token);
       var userObj = {};
        userObj['user_id'] = localStorage.getItem('user_id');
        var userJsonString = JSON.stringify(userObj);
        // wip(1);
        var accessToken = localStorage.getItem('access_token');
        this.idleLogout();
      }, reason => {
        //   wip(0);
        //this.alert(reason.error.message,'danger');
        //this.data.idleLogout();
        location.reload();
        this.logout();
      });
  }
  restart() {
    this.userIdle.resetTimer();
  }
  onTimeout() {
    this.userIdle.onTimeout();
  }
  readable_timestamp(t) {
    // Split timestamp into [ Y, M, D, h, m, s ]
    var t: any = t.split(/[- :]/);

    // Apply each element to the Date function
    var d = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));

    //handle parsing GMT as UTC
    d.setMinutes(d.getMinutes() - 330);

    t = d.toTimeString();
    t = t.split(" ");
    return d.toDateString() + " " + t[0];
  }

  logout() {
  
    localStorage.clear();
    //caches.delete;
    //location.reload();
    this.route.navigateByUrl('/login');
  }

  alert(msg, type, time = 3000) {
    //console.log(msg, type);
    this.reason = msg;
    this.icon = 'puff';

    if (msg == 'Loading...') {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
      }, 10000);
    } else {
      $('.alert:first').fadeOut();
      var htx = `<div class="alert alert-` + type + ` my-2" role="alert">` + msg + `</div>`;
      $('.alertPlace').append(htx).fadeIn();
      setTimeout(() => {
        $('.alert:last').remove().fadeOut();
      }, time);
    }
  }

}
